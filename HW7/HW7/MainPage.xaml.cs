﻿using System;
using Xamarin.Forms;


namespace HW7
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //Practice OnApprearing function
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            list_ToDoList.ItemsSource = await App.Database.GetNotesAsync();
        }

        //Take action fron user when the user clicks on the PLUS button adding the task
        async void OnClickAction(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EntryPage
            {
                BindingContext = new Note()
            });
        }

        //Display the list of tasks needs to be done.
        async void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new EntryPage
                {
                    BindingContext = e.SelectedItem as Note
                });
            }
        }
    }
}