﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;


namespace HW7
{
    public class NoteDatabase
    {
        readonly SQLiteAsyncConnection data;

        public NoteDatabase(string dbPath)
        {
            data = new SQLiteAsyncConnection(dbPath);
            data.CreateTableAsync<Note>().Wait();
        }

        public Task<List<Note>> GetNotesAsync()
        {
            return data.Table<Note>().ToListAsync();
        }

        public Task<Note> GetNoteAsync(int id)
        {
            return data.Table<Note>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        public Task<int> SaveNoteAsync(Note note)
        {
            if (note.ID != 0)
            {
                return data.UpdateAsync(note);
            }
            else
            {
                return data.InsertAsync(note);
            }
        }

        public Task<int> DeleteNoteAsync(Note note)
        {
            return data.DeleteAsync(note);
        }
    }
}