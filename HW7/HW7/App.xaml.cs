﻿using System;
using System.IO;
using Xamarin.Forms;

namespace HW7
{
    public partial class App : Application
    {
        static NoteDatabase noteData;

        public static NoteDatabase Database
        {
            get
            {
                if (noteData == null)
                {
                    noteData = new NoteDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "NoteData.db3"));
                }
                return noteData;
            }
        }

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());
        }
    }
}