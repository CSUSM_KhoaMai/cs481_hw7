﻿using System;
using SQLite;

namespace HW7
{
    //initilize data field needed for the task
    public class Note
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; } //name of the task
        public string Text { get; set; } //content of the task
        public DateTime Date { get; set; } //date add the task
    }
}